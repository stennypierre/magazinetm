<?php
session_start();
    if(isset($_SESSION['users'])){
        header("Location:magazineTM-accueil-bienvenue");
    }
    require('functions.php');

    if(isset($_POST['submit'])){
        $dt=$_POST['date'];
        $ttr=$_POST['titre'];
        $aut=$_POST['auteur'];
        $pht= addsLashes($_FILES['uploadphoto']['tmp_name']) ;
        $pht = file_get_contents($pht);
        $img = base64_encode($pht);
        $desc=$_POST['description'];
        newArticles($dt, $ttr, $aut, $img, $desc);
    }

    $nb = getTotalArticles();
?>
<!DOCTYPE html>
<html class="no-js"> 
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>BackOffice-MagazineTM</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="A.Pierre Stenny" />
    

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="favicon.ico">
	<!-- Animate -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon -->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">

    <link rel="stylesheet" href="css/style.css">
    <script src="ckeditor/ckeditor.js"></script>
    <script src="ckeditor/styles.js"></script>


	

	</head>
	<body class="container">
        <h2 style="text-align:center;">BackOffice</h2>

        <div class="alert alert-info">
            <i class="icon icon-info-circle icon-lg"></i>
            <strong>Info !</strong> Articles publi&eacute;e :<i><?php echo $nb ?></i>.
        </div>
	
        <form action="#" method="post" enctype="multipart/form-data">
					<div class="form-group">
                        <input type="text" class="form-control" name="date" placeholder="Date">
                        <input type="text" class="form-control" name="titre" placeholder="Titre">
                        <input type="text" class="form-control" name="auteur" placeholder="Auteur">
                        <input type="file" name="uploadphoto" id="exampleInputFile">
                        <textarea name="description" class="ckeditor" placeholder="Description"></textarea>
                        <button type="submit" name="submit" >Publier</button>
					</div>
				</form>
	


	

        <?php 
            include('footer.php');
            include('loadjs.php');
        ?>


	</body>
</html>

