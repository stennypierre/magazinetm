<?php
    require('functions.php');
    $art = get_all_articles();
    $nb = getTotalArticles();
?>
<!DOCTYPE html>
<html class="no-js"> 
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>MagazineTM-Accueil</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Magasine qui parle de la culture dans chaques pays" />
	<meta name="keywords" content="Culture, Musique, Tradition, Origine, ..." />
    <meta name="author" content="A.Pierre Stenny" />
    
	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="favicon.ico">
	<!-- Animate -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon -->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">

	<link rel="stylesheet" href="css/style.css">


	

	</head>
	<body>
        <?php include('header.php'); ?>
	

        <div class="container-fluid">
		<div class="row fh5co-post-entry">
			
			
        <?php foreach($art as $key){ ?>
			
			
			<article class="col-lg-3 col-md-3 col-sm-3 col-xs-6 col-xxs-12 animate-box">
				<figure>
					<a href="articles-<?php echo $key['id'] ?>.html"><img src="data:image;base64,<?php echo $key['photo']; ?>" alt="" class="img-responsive"></a>
				</figure>
			
				<h2 class="fh5co-article-title"><a href="articles-<?php echo $key['id'] ?>.html"><?php echo $key['titre']; ?></a></h2>
				<span class="fh5co-meta fh5co-date"><?php echo $key['datePub']; ?></span>
			</article>
            <div class="clearfix visible-xs-block"></div>
            
        <?php } ?>
		</div>
	</div>


	

        <?php 
            include('footer.php');
            include('loadjs.php');
        ?>


	</body>
</html>

