<?php
    require('functions.php');
    $valeur = $_GET['id'];
    $det = getDetailArticles($valeur);
?>
<!DOCTYPE html>
<html class="no-js"> 
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo $det[0]['titre'] ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Magasine qui parle de la culture dans chaques pays" />
	<meta name="keywords" content="Culture, Musique, Tradition, Origine, ..." />
	<meta name="author" content="A.Pierre Stenny" />

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="favicon.ico">
	<!-- Animate -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon -->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">

	<link rel="stylesheet" href="css/style.css">


	

	</head>
	<body>
        <?php include('header.php'); ?>
	

        <div class="container-fluid">
		<div class="row fh5co-post-entry single-entry">
			<article class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12 col-xs-offset-0">
				<figure class="animate-box">
					<img src="data:image;base64,<?php echo $det[0]['photo']; ?>" alt="Image" class="img-responsive">
				</figure>
				
				<h2 class="fh5co-article-title animate-box"><?php echo $det[0]['titre']; ?></h2>
				<span class="fh5co-meta fh5co-date animate-box"><?php echo $det[0]['datePub']; ?></span>
				
				<div class="col-lg-12 col-lg-offset-0 col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0 text-left content-article">
					<div class="row">
						<div class="col-lg-8 cp-r animate-box">
                            <p>
                                <?php echo $det[0]['description']; ?>
                            </p>
                            <p>
                                <cite><?php echo $det[0]['auteur']; ?></cite>
                            </p>

							
						</div>
						
					</div>

					

					

					
					
				</div>
			</article>
		</div>
	</div>



	

        <?php 
            include('footer.php');
            include('loadjs.php');
        ?>


	</body>
</html>

